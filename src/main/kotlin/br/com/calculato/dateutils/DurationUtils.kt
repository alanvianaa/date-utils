package br.com.calculato.dateutils

import java.time.Duration

/**
 * Helper method to converts [this] in duration as minutes unites
 */
fun Short.toMinutesOfDuration() = Duration.ofMinutes(this.toLong())